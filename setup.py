# -*- coding: utf-8 -*-
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'dateutils',
    'jinja2',
    "pillow"
]

setup(name='CV Generator',
      version='0.1.0',
      description='CV Generator',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
      ],
      author='Łukasz Bołdys',
      author_email='mail@utek.pl',
      url='http://dev.utek.pl',
      keywords='cv generator',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='cvutk',
      install_requires=requires,
      entry_points="""\
      [console_scripts]
      cv-generate = cvutk.scripts.generate:main
      """,
      )
