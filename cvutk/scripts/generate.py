import argparse
import os
import shutil
from distutils.dir_util import copy_tree
import errno
import json
from jinja2 import Environment, PackageLoader
import PIL.Image

env = Environment(loader=PackageLoader('cvutk', 'templates'))

parser = argparse.ArgumentParser(description="Generates CV")
parser.add_argument(
    'data_file', type=str, help="path to the file with json data")


def copy(src, dst):
    try:
        # using distutils.dir_util.copy_tree because shutil one did not
        # support overwrite
        copy_tree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            raise


def create_dir(dirname):
    try:
        os.mkdir(dirname)
    except OSError:
        pass


def resize(image_path, size, output_path):
    image = PIL.Image.open(image_path)
    (original_width, original_height) = image.size
    (target_width, target_height) = size

    if original_width <= target_width and original_height <= target_height:
        return image
    horz_scale = float(target_width) / original_width
    vert_scale = float(target_height) / original_height
    if horz_scale < vert_scale:
        final_size = (target_width, int(original_height * horz_scale))
    else:
        final_size = (int(original_width * vert_scale), target_height)
    if image.mode in ["1", "P"]:
        image = image.convert("RGB")
    image = image.resize(final_size, PIL.Image.ANTIALIAS)
    image.save(output_path, image.format, quality=90)


def process_photo(photo_path, build_path):
    images_path = os.path.join(build_path, "img")
    create_dir(images_path)
    resize(photo_path, (250, 300), os.path.join(
        images_path, os.path.basename(photo_path)))
    path = os.path.join("img", os.path.basename(photo_path))
    path = path.replace("\\", "/")
    return path


def main():
    args = parser.parse_args()
    data_file = args.data_file
    build_path = "build"
    with open(data_file) as fp:
        data = json.load(fp)
    create_dir(build_path)
    photo_path = data.get("basic_info", {"photo": None}).get("photo", None)
    if photo_path is not None:
        photo_path = os.path.join(os.path.dirname(data_file), photo_path)
        data["basic_info"]["photo"] = process_photo(photo_path, build_path)

    print data["basic_info"]
    template = env.get_template("small/index.html")
    generated_data = template.render(cv=data).encode("utf-8")

    for _dir in os.listdir("cvutk/templates/small/static/"):
        copy(os.path.join("cvutk/templates/small/static/", _dir),
             os.path.join(build_path, _dir))
    with open(os.path.join(build_path, "index.html"), "w") as fp:
        fp.write(generated_data)
